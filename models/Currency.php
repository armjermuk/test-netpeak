<?php

/**
 * Currency
 */
abstract class Currency
{
    /**
     * Currencies
     */
    protected $baseCurrency;
    protected $currentCurrency;
    protected $ratio = 1;

    protected function setBaseCurrency($currency) {
        $this->baseCurrency = $currency;
    }

    protected function getBaseCurrency() {
        return $this->baseCurrency;
    }

    public static function currenciesList() {
        $currencies = array('AUD' => 'Австралийский доллар', 'AZN' => 'Азербайджанский манат', 'GBP' => 'Фунт стерлингов Соединенного королевства', 'AMD' => 'Армянских драмов', 'BYN' => 'Белорусский рубль', 'BGN' => 'Болгарский лев', 'BRL' => 'Бразильский реал', 'HUF' => 'Венгерских форинтов', 'HKD' => 'Гонконгских долларов', 'DKK' => 'Датская крона', 'USD' => 'Доллар США', 'EUR' => 'Евро', 'INR' => 'Индийских рупий', 'KZT' => 'Казахстанских тенге', 'CAD' => 'Канадский доллар', 'KGS' => 'Киргизских сомов', 'CNY' => 'Китайских юаней', 'MDL' => 'Молдавских леев', 'NOK' => 'Норвежских крон', 'PLN' => 'Польский злотый', 'RON' => 'Румынский лей', 'XDR' => 'СДР (специальные права заимствования)', 'SGD' => 'Сингапурский доллар', 'TJS' => 'Таджикских сомони', 'TRY' => 'Турецкая лира', 'TMT' => 'Новый туркменский манат', 'UZS' => 'Узбекских сумов', 'UAH' => 'Украинских гривен', 'CZK' => 'Чешских крон', 'SEK' => 'Шведских крон', 'CHF' => 'Швейцарский франк', 'ZAR' => 'Южноафриканских рэндов', 'KRW' => 'Вон Республики Корея', 'JPY' => 'Японских иен',);
        return $currencies;
    }

}

?>