<?php
/**
 * Центральный банк России
 */
class cbr extends Currency implements ICurrency
{
    private static $instance = null;

    /**
     *
     * Constructor
     *
     * @return none
     */
    public function __construct($currency,$baseCurrency)
    {
        $this->currentCurrency = $currency;
        $this->baseCurrency = $baseCurrency;
        $this->setBaseCurrency($this->baseCurrency);
    }

    public function getRatio($items) {
        foreach ($items as $item)
        {
            $code = $item->getElementsByTagName('CharCode')->item(0)->nodeValue;
            $curs = $item->getElementsByTagName('Value')->item(0)->nodeValue;
            $name = $item->getElementsByTagName('Name')->item(0)->nodeValue;
            $nominal = $item->getElementsByTagName('Nominal')->item(0)->nodeValue;
            $nominal = str_replace(',', '.', $nominal);

            if(strtoupper($code) == 'USD') {
                $this->ratio = $curs;
                $this->ratio = str_replace(',', '.', $this->ratio);
                $this->ratio = floatval($this->ratio) / $nominal;
            }
        }
    }

    public function convert($currency = null, $baseCurrency = null) {
        if($currency) $this->currentCurrency = $currency;
        if($baseCurrency) $this->baseCurrency = $baseCurrency;

        $url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . date('d.m.Y');

        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
        if (curl_errno($ch)) {
            echo curl_error($ch);
            echo "\n<br />";
            $contents = '';
        } else {
            curl_close($ch);
        }

        if (!is_string($contents) || !strlen($contents)) {
            echo "Failed to get contents.";
            $contents = '';
        }


        if ($contents) {
            $xml = DOMDocument::loadXML($contents);

            $root = $xml->documentElement;
            $items = $root->getElementsByTagName('Valute');
            $ratio = $this->getRatio($items);

            foreach ($items as $item)
            {
                $code = $item->getElementsByTagName('CharCode')->item(0)->nodeValue;
                $name = $item->getElementsByTagName('Name')->item(0)->nodeValue;
                $curs = $item->getElementsByTagName('Value')->item(0)->nodeValue;
                $nominal = $item->getElementsByTagName('Nominal')->item(0)->nodeValue;

                if(strtoupper($code) == strtoupper($this->currentCurrency)) {
                    $curs = str_replace(',', '.', $curs);
                    $nominal = str_replace(',', '.', $nominal);
                    $value = floatval($curs) / $nominal / $this->ratio;
                    $value = number_format((float)$value, 5, '.', '');
                    $json = $this->jsonData($value, $code, $name);
                    return $json;
                }
            }
        }
    }

    public function jsonData($value, $code, $name) {
        $arr = [
            'status' => 'success',
            'name' => $name,
            'code' => $code,
            'value' => $value,
        ];

        return json_encode($arr);
    }

    /**
     *
     * Get singleton instance
     *
     * @return object  instance of this class
     */
    public static function getInstance()
    {
        return (self::$instance ? self::$instance : self::$instance = new self());
    }

    /**
     *
     * Clone
     *
     * @return none
     */
    private function __clone()
    {
    }

    public static function getCurrencies($date = null) {
        if (!isset($date)) $date = date("Y-m-d");
        $client = new \SoapClient("http://api.cba.am/exchangerates.asmx?WSDL");

        $date = date("Y-m-d");
        $curs = $client->ExchangeRatesByDate(array("date" => $date));

        $rates = $curs->ExchangeRatesByDateResult->Rates->ExchangeRate;

        $arr = array();
        foreach ($rates as $rate){
            $arr[] = $rate;
        }

        return $arr;
    }


}

?>