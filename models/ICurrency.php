<?php

interface ICurrency
{
    public function convert($currency = null, $baseCurrency = null);
    public function getRatio($items);
    public function jsonData($value, $code, $name);
}

?>