<?php
header('Content-Type: text/html; charset=utf-8');
require_once 'vendor/autoload.php';


class Controller
{
    public $load;
    public $model;
    public $data = Array();
    public $userMgr;

    function __construct()
    {
        set_include_path(implode(PATH_SEPARATOR, array(realpath('./models'), get_include_path(),)));

        include 'autoloader.php';
        $this->load = new load();

        if (isset($_REQUEST['action']) && $_REQUEST['action']) {
            return $this->__call($_REQUEST['action'], array());
        } else {
            return $this->index();
        }
    }

    /*  для динамического подключения страниц  */
    public function __call($name, $params)
    {
        if (true == method_exists($this, $name)) {
            call_user_func_array(array(&$this, $name), $params);
        }
    }

    function __autoload($class_name)
    {
        include $class_name . '.php';
    }

    function index()
    {
        $data = Array();
        $data['list'] = Currency::currenciesList();

        $this->load->view('index.php', $data);
    }

    function convert() {
        $provider = (isset($_POST['provider'])) ? $_POST['provider'] : '';
        $currency = (isset($_POST['currency'])) ? $_POST['currency'] : '';
        $obj = '';

        if($provider && $currency) {
            switch ($provider) {
                case 'cbr':
                    $obj = new cbr($currency, $provider);
                    echo $obj->convert();
            break;
            }
        }
    }

}

?>
