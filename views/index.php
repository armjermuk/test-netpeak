<?php
$this->view('header.php');
?>
    <section>
        <div class="menuDiv">
            <div class="col-xs-8 col-md-8 homeLink"><a href="/">Главное</a></div>
        </div>
        <div class="clearfix">&nbsp;</div>
    </section>

    <section>
        <div class="container">
            <div class="searchDiv">
                <h3>Cервис конвертации доллара в другую валюту</h3>
                <select id="provider">
                    <option value="cbr">Цетральный банк России</option>
                </select>

                <select id="currency">
                    <option value="">Выбрать</option>
                    <?php foreach ($data['list'] as $key => $value): ?>
                        <option value="<?php echo $key ?>"><?php echo $value ?></option>
                    <?php endforeach; ?>
                </select>
                <input id="convert" class="btn-primary" type="button" value="Convert">
            </div>
            <hr>
            <div id="result"></div>
        </div>
    </section>

    <!-- Modal -->
    <div id="modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Информация</h4>
                </div>
                <div class="modal-body">
                    <h3>Some text in the modal.</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

<?php $this->view('footer.php', $data); ?>