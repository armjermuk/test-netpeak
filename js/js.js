$(document).ready(function () {
    $("#convert").on("click", function () {
        var currency = $('#currency').val();
        var provider = $('#provider').val();

        if(jQuery.trim(currency) == '') {
            $("#modal").find('.modal-body').html('Не выбрана валюта!');
            $("#modal").modal('show');
            return;
        }
        
        var data = {
            currency: currency,
            provider: provider
        }
        
        $.ajax({
            method: 'POST',
            url: '?action=convert',
            data: data,
            success: function (data) {
                var obj = jQuery.parseJSON(data);

                if (obj.status == 'success') {
                    var str;
                    str = '1 ' + obj.name + ': ' + obj.value + ' USD';
                    $('#result').html(str);
                } else {
                    var str = 'Не предвиденная ошибка!';
                    alert(str);
                }
            }
        
        });
    })

})



